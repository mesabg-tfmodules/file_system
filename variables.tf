variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "EFS name"
}

variable "subnet_ids" {
  type        = list(string)
  description = "Subnet identifier list"
}

variable "security_groups" {
  type        = list(string)
  description = "Security Groups List"
}

variable "path" {
  type        = string
  description = "Path to be used"
  default     = "/data"
}

variable "chmod" {
  type        = number
  description = "Ownership"
  default     = 755
}

variable "persistent" {
  type        = bool
  description = "Persist volume"
  default     = false
}
