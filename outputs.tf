output "file_system_id" {
  value       = aws_efs_file_system.file_system.id
  description = "Created file system id"
}

output "file_system_dns_name" {
  value       = aws_efs_file_system.file_system.dns_name
  description = "Created file system dns name"
}

output "access_point_id" {
  value       = aws_efs_access_point.access_point.id
  description = "Created access point id"
}
