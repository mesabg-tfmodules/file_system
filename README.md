# EFS Module

This module is capable to generate an EFS.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general EFS name
- `subnet_id` - subnet identifier
- `security_groups` - security groups to be used
- `path` - base path (default /data)
- `chmod` - base ownership (default 755)

Usage
-----

```hcl
module "file_system" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/file_system.git"

  environment         = "environment"
  name                = "name"

  subnet_ids          = ["subnet-xxxx", "subnet-aaaa"]
  security_groups     = ["sg-aaaa", "sg-bbbb"]
  path                = "/some/path"
}
```

Outputs
=======

 - `file_system_id` - Created EFS id
 - `access_point_id` - Created Access Point id


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
