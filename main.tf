terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.13.0"
    }
  }
}

resource "aws_efs_file_system" "file_system" {
  creation_token      = var.name
  encrypted           = true

  lifecycle_policy {
    transition_to_ia  = "AFTER_30_DAYS"
  }

  throughput_mode     = "bursting"
  performance_mode    = "generalPurpose"

  tags = {
    Name              = var.name
    Environment       = var.environment
  }
}

resource "aws_efs_mount_target" "mount_target" {
  count           = length(var.subnet_ids)

  file_system_id  = aws_efs_file_system.file_system.id
  subnet_id       = var.subnet_ids[count.index]
  security_groups = var.security_groups
}

resource "aws_efs_access_point" "access_point" {
  file_system_id  = aws_efs_file_system.file_system.id

  posix_user {
    gid           = 0
    uid           = 0
  }

  root_directory {
    path = var.path
    creation_info {
      owner_gid   = 0
      owner_uid   = 0
      permissions = var.chmod
    }
  }

  tags = {
    Name          = var.name
    Environment   = var.environment
  }
}
